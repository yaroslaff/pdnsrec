#!/usr/bin/python3

import argparse
import MySQLdb
import MySQLdb.cursors
import sys
import time
import os
import re

import peewee as pw

from dotenv import load_dotenv

load_dotenv()

db = pw.MySQLDatabase(None)

class Domain(pw.Model):
    class Meta:
        db_table = 'domains'
        database = db 
    name = pw.CharField()

class Record(pw.Model):
    class Meta:
        db_table = 'records'
        database = db
    domain = pw.ForeignKeyField(Domain)
    name = pw.CharField()
    type = pw.CharField()
    ttl = pw.IntegerField(default=86400)
    prio = pw.IntegerField(default=0)
    content = pw.CharField()
    change_date = pw.IntegerField()
    ordername = pw.CharField()
    auth = pw.IntegerField(default=1)

    def save(self, *args, **kwargs):
        self.change_date = int(time.time())
        return super(Record, self).save(*args, **kwargs)

def do_delete(db,args):
    t = args.delete[0]
    name = args.delete[1]
    print("delete type: {} name: {}".format(t,name))
    query = Record.delete().where(Record.name == name, Record.type == t)
    print("deleted {} records".format(query.execute()))

def do_fix(db, domainname):
    defsoa = ['ns1.accomazzi.net.', 'domini.accomazzi.net.' ,'0', '10800', '3600', '604800', '3600']
    d = Domain.get(name=domainname)
    try:
        r = Record.get(domain_id = d.id, type='SOA')
    except Record.DoesNotExist:
        print("NO SOA FOR", domainname)
        soaline = ' '.join(defsoa)
        print(soaline)
        r = Record.create(domain=d, type='SOA', name=domainname, content=' '.join(defsoa))
        r.save()
        do_touch(db, domainname)
        print(r.content)

    soa = r.content.split(' ')
    if len(soa) != 7:
        newsoa = list(defsoa)
        for pos, f in enumerate(soa):
            newsoa[pos] = f
        r.content = ' '.join(newsoa)
        print("new SOA for #{} {}: {}".format(repr(d.id), d.name, r.content))
        r.save()
        do_touch(db, domainname)
    else:
        print("Domain #{} {} SOA OK".format(repr(d.id), repr(d.name)))

def do_exam(db,domainname):

    d = Domain.get(name=domainname)

    print("Domain #{} {}".format(repr(d.id), repr(d.name)))
    
    for rec in d.record_set:
        print("name: {} content: {} change_date: {}".format(
            repr(rec.name),repr(rec.content),repr(rec.change_date)
            ))

        if rec.content is not None:
            if rec.content != rec.content.strip():
                print("ERROR! Domain {} {}, record {} {}, content {}".format(
                    d.id, d.name,
                    rec.id, rec.name,
                    repr(rec.content)
                ))
        else:
            print("WARN", rec.name,"is", rec.content)



def do_touch(db,domainname):

    d = Domain.get(name=domainname)
    
    soarec = Record.get(Record.type=='SOA',Record.name==domainname)
    soaf = [_f for _f in soarec.content.split(' ') if _f] # get SOA as list, fixing extra spaces

    now = int(time.strftime('%Y%m%d00'))
    if now<=int(soaf[2]):
        soaf[2]=str(int(soaf[2])+1)
    else:
        soaf[2]=str(now)
    soarec.content = ' '.join(soaf)

    soarec.save()
    
    #timestr = int(time.time())
    #for rec in d.record_set:
    #    rec.change_date = timestr
    #    rec.save()


def do_seta(db,args):
    fullname=args.seta[0]
    ip=args.seta[1]


    if fullname.count('.') == 1:
        # domain name
            newname = ''
            domainname = fullname
    else:
        # host + domain name
        m = re.match('([^\.]+)\.([^\.]+\.[^\.]+)$',fullname)
        if m:
            newname = m.group(1)
            domainname = m.group(2)
        else:
            print("cannot parse hostname")
            return

   
    try:
        arec = Record.get(Record.type=='A',Record.name==fullname)
        print("old record {} -> {} ({})".format(fullname,arec.content,arec.change_date))
        arec.content = ip
        arec.save()
        print("new record {} -> {} ({})".format(fullname,arec.content,arec.change_date))        
        # do_touch(db,domainname)
        return
    except Record.DoesNotExist:
        print("no such record yet")
   
    try:
        d = Domain.get(Domain.name == domainname)
        print("domain:",d.name)
        print("id:",d.id)
    except Domain.DoesNotExist:
        print("sorry, no such domain: '{}'".format(domainname))

    # now we have domain and must create record
    arec = Record.create(ordername=newname, domain=d, type='A', name=fullname, content=ip)
    print("created, id: {}".format(arec.id))
    # do_touch(db,domainname)


def do_setttl(db,args):
    fullname=args.setttl[0]
    ttl=int(args.setttl[1])

    try:
        arec = Record.get(Record.type=='A', Record.name==fullname)
        print("old record {} -> {} ({})".format(fullname,arec.content, arec.ttl, arec.change_date))
        arec.ttl = ttl
        arec.save()
        print("new record {} -> {} ({})".format(fullname,arec.content, arec.ttl, arec.change_date))        
        # do_touch(db,domainname)
        return
    except Record.DoesNotExist:
        print("no such record yet")


def do_a(db,a):
    print("resolving",a)
    cur=db.cursor()    
    cur.execute("SELECT * FROM records WHERE type='A' AND name='%s'" % a);    
    a = cur.fetchone()
    print(a)
    # print "name: {name}".format(**a)
 #   print "%(name)s %(content)s" % cur.fetchone()
 
def do_list(db,args):
    cur=db.cursor()    
    cur.execute("SELECT * FROM domains");    
    for row in cur.fetchall():
        print(row['name'])

def do_zone(db, zone):
    d = Domain.get(name=zone)

    print("Domain #{} {}".format(repr(d.id), repr(d.name)))

    for rec in d.record_set:
        print("{} {} {} {}".format(
            repr(rec.name), repr(rec.type), repr(rec.content), repr(rec.change_date)
        ))


def do_ip(db,args):
    ip=args.ip
    print("Listing",ip)
    query = "SELECT * FROM records WHERE content='%s'" % ip
    print(query)
    cur = db.cursor()
    cur.execute(query)
    for row in cur.fetchall():
        print("%(name)s %(type)s %(content)s" % row)

def do_trans(db,args):
    (old,new) = args.trans
    print("trans %s -> %s" % (old,new))
    cur = db.cursor()
    query = "SELECT domain_id FROM records WHERE content='%s' GROUP BY domain_id" % old
    affected=[]
    print(query)
    cur = db.cursor()
    cur.execute(query)
    for row in cur.fetchall():
        affected.append(row['domain_id'])

    query="UPDATE records SET content='%s', change_date=UNIX_TIMESTAMP() WHERE content='%s'" % (new,old)
    cur.execute(query)
    for a in affected:
        # get name for this domain
        cur.execute("SELECT name FROM domains WHERE id=%d" % a);
        row=cur.fetchone()
        name=row['name']
        print("update serial",name)
        do_touch(db,name)
    db.commit()
 


def do_touch_old(db,domain):

    cur=db.cursor()
    cur.execute("SELECT * FROM records WHERE name='%s' and type='soa'" % domain)
    oldsoa = cur.fetchone()
    content=oldsoa['content'];
    csplit=content.split();
    serial=csplit[2];
 
    newserial = time.strftime("%Y%m%d00")

    if newserial <= serial:
        newserial = int(serial)+1

    #(master,hostmaster,serial,refresh,retry,expiry,nxdomain)=content.split();
    csplit[2]=str(newserial);
    newcontent = " ".join(csplit)
    q="UPDATE records SET content='%s',change_date=UNIX_TIMESTAMP() WHERE id=%d" % (newcontent,oldsoa['id'])
    cur.execute(q)
    db.commit()


def main():
    global db
    p = argparse.ArgumentParser(description="PowerDNS record management",fromfile_prefix_chars='@')
    p.add_argument("--user", dest='dbuser', help="mysql user", default = os.getenv('DBUSER'))
    p.add_argument("--pass", dest='dbpass', help="mysql pass", default=os.getenv("DBPASS"))
    p.add_argument("--name", dest='dbname', help="mysql db name", default=os.getenv("DBNAME"))
    p.add_argument("--ip", help="show all records for this IP")
    p.add_argument("--list", action='store_true',help="list all zones");
    p.add_argument("--zone", metavar='zonename', help="list records in zone");
    p.add_argument("--a", help='resolve by database', metavar='hostname');
    p.add_argument("--seta", nargs=2, metavar=('name','ip'),help="set A record")
    p.add_argument("--setttl", nargs=2, metavar=('name','ttl'),help="set TTL for A record")
    p.add_argument("--delete", nargs=2, metavar=('type','name'),help="delete record")        
    p.add_argument("--trans", nargs=2, metavar=('oldip','newip'),help="translate records from one IP to other")
    p.add_argument("--touch", metavar='zonename',help="update serial for domain")
    p.add_argument("--exam", metavar='zonename',help="examine domain")
    p.add_argument("--fix", metavar='zonename',help="check and fix domain")



    argv=sys.argv[1:]
    if os.path.exists('pdnsrec.conf'):
        argv = ['@pdnsrec.conf'] + argv
    args = p.parse_args(argv)

    if args.dbuser and args.dbname and args.dbpass:
    
        if args.seta or args.setttl or args.touch or args.delete \
                    or args.exam or args.fix or args.zone:
            try:
                db.init(args.dbname, user=args.dbuser, passwd=args.dbpass)
                db.connect()
            except pw.OperationalError as e:
                print("Connect failed:", str(e))
                sys.exit(1)
        else:
            # old methods
            try:
                db = MySQLdb.connect(host="localhost",user=args.dbuser,passwd=args.dbpass,db=args.dbname,cursorclass=MySQLdb.cursors.DictCursor);
            except MySQLdb.OperationalError as ex:
                print("Operational error", str(ex))
                # print(ex[1])
                sys.exit(1)
            except Exception as ex:
                ename =     ex.__class__.__name__
                print("ename:",ename)
                sys.exit(1)
         
        
        if args.delete:
            do_delete(db,args)
            
        if args.seta:
            do_seta(db,args)

        if args.setttl:
            do_setttl(db,args)

        if args.ip:
            do_ip(db,args)

        if args.list:
            do_list(db,args)
        
        if args.trans:
            do_trans(db,args)
        
        if args.touch:
            do_touch(db,args.touch)

        if args.exam:
            do_exam(db,args.exam)

        if args.fix:
            do_fix(db,args.fix)


        if args.a:
            do_a(db,args.a)

        if args.zone:
            do_zone(db, args.zone)

    else:
        print("need to know user/pass/dbname and command")
        sys.exit(0)

main()


