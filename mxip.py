#!/usr/bin/python

import dns.resolver
import sys

domain = sys.argv[1]

try:
    for mxanswer in dns.resolver.query(domain, 'MX'):
        mx = mxanswer.exchange
        
        for ipanswer in  dns.resolver.query(mx, 'A'):
            print domain,mx,ipanswer
            
except (dns.resolver.NoAnswer, dns.resolver.NXDOMAIN) as e:
    print domain,"EXCEPTION",str(e)
    
